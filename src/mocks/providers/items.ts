import { Injectable } from '@angular/core';

import { Item } from '../../models/item';

@Injectable()
export class Items {
  items: Item[] = [];

  defaultItem: any = {
    "name": "Burt Bear",
    "profilePic": "assets/img/speakers/bear.jpg",
    "cost": "$5.000",
    "about": "Burt is a Bear.",
  };


  constructor() {
    let items = [
      {
        "name": "Afeitado al ras",
        "profilePic": "assets/img/speakers/bear.jpg",
        "cost": "$5.000",
        "about": "20 minutos"
      },
      {
        "name": "Afeitado al ras toallas calientes",
        "profilePic": "assets/img/speakers/cheetah.jpg",
        "cost": "$7.000",
        "about": "30 minutos"
      },
      {
        "name": "Alisado Permanente",
        "profilePic": "assets/img/speakers/duck.jpg",
        "cost": "$40.000",
        "about": "120 minutos"
        
      },
      {
        "name": "Balayage Cabello corto",
        "profilePic": "assets/img/speakers/eagle.jpg",
        "cost": "$40.000",
        "about": "90 minutos"
      },
      {
        "name": "Balayage Cabello largo",
        "profilePic": "assets/img/speakers/elephant.jpg",
        "cost": "$55.000",
        "about": "90 minutos"
      },
      {
        "name": "Balayage Cabello medio",
        "profilePic": "assets/img/speakers/mouse.jpg",
        "cost": "$45.000",
        "about": "90 minutos"
      },
      {
        "name": "Balayage Cabello XL",
        "profilePic": "assets/img/speakers/puppy.jpg",
        "cost": "$60.000",
        "about": "90 minutos"
      }
    ];

    for (let item of items) {
      this.items.push(new Item(item));
    }
  }

  query(params?: any) {
    if (!params) {
      return this.items;
    }

    return this.items.filter((item) => {
      for (let key in params) {
        let field = item[key];
        if (typeof field == 'string' && field.toLowerCase().indexOf(params[key].toLowerCase()) >= 0) {
          return item;
        } else if (field == params[key]) {
          return item;
        }
      }
      return null;
    });
  }

  add(item: Item) {
    this.items.push(item);
  }

  delete(item: Item) {
    this.items.splice(this.items.indexOf(item), 1);
  }
}
