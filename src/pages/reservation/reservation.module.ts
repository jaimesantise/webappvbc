import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ReservationPage } from './reservation';
import { NgCalendarModule } from 'ionic2-calendar';


@NgModule({
  declarations: [
    ReservationPage,
  ],
  imports: [
    NgCalendarModule,
    IonicPageModule.forChild(ReservationPage),
  ],
  exports: [
    ReservationPage,
  ]
})
export class ReservationPageModule {}
